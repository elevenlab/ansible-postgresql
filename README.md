Ansible Role: PostgreSQL
===================
Install and configure PostgreSQL as stand alone database on a Debian server.

It will configure postrgreSQL, set the allowed connection, set the admin password, create users and databases.

If you want to setup a databse cluster use instead [postgreSQL-replica](https://bitbucket.org/elevenlab/ansible-postgresql-replica).

----------

Role variables
-------------
Available variables are listed below, along with default values (see `defaults/main.yml`):

	pg_version: '9.6'
	pg_cluster_name: 'main'
	pg_port: 5432
The `pg_version` defaults to `9.6` but for Debian 8 `9.4` is needed.  `pg_cluster_name` is the name of the *pgsql* cluster (database data will be stored in `/var/lib/postgresql/{{pg_version}}/{{pg_cluster_name}}/`). The `pg_port` is the port where the database is listening.

	pg_shared_buffers: 256MB

As reported in the postgreSQLl [documentation](https://www.postgresql.org/docs/9.6/static/runtime-config-resource.html#GUC-SHARED-BUFFERS):
>[`pg_shared_buffers`] Sets the amount of memory the database server uses for shared memory buffers. The default is typically 128 megabytes (128MB), but might be less if your kernel settings will not support it (as determined during initdb). This setting must be at least 128 kilobytes. (Non-default values of BLCKSZ change the minimum.) However, settings significantly higher than the minimum are usually needed for good performance. This parameter can only be set at server start.

>If you have a dedicated database server with 1GB or more of RAM, a reasonable starting value for shared_buffers is 25% of the memory in your system. There are some workloads where even large settings for shared_buffers are effective, but because PostgreSQL also relies on the operating system cache, it is unlikely that an allocation of more than 40% of RAM to shared_buffers will work better than a smaller amount. Larger settings for shared_buffers usually require a corresponding increase in checkpoint_segments, in order to spread out the process of writing large quantities of new or changed data over a longer period of time.

>On systems with less than 1GB of RAM, a smaller percentage of RAM is appropriate, so as to leave adequate space for the operating system. Also, on Windows, large values for shared_buffers aren't as effective. You may find better results keeping the setting relatively low and using the operating system cache more instead. The useful range for shared_buffers on Windows systems is generally from 64MB to 512MB.


	pg_restart_after_crash: 'on'
If something went wrong as default the postgreSQL server will restart automatically. 

	pg_accept_connection: []
A list of connections that the server should allow. each element has the following required fields:
	
	hosts: []
	database:
	user:
	mask:
	type:
When the hosts provided are in the form of ip addresses you must specify the `mask` in the [CIDR](https://tools.ietf.org/html/rfc4632) notation: e.g:`/32` means the single host. If the host are provided in the form of hostname that the server can resolve (typically FQDN), you must set `mask: ''`.  `type` is the type of auth, typically `md5` is the expected value.
for further details look the postgreSQL documentation of [`pg_hba`](https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html)

	pg_users: []
A list of users (roles) that you want available in the database. Each user must have the fileds: `username`, `password` and `role`. The last one is a role attributes: a string in the format `'val1,val2'`. All of these value are allowed:
		
* [NO]SUPERUSER
* [NO]CREATEROLE
* [NO]CREATEUSER
* [NO]CREATEDB
* [NO]INHERIT
* [NO]LOGIN
* [NO]REPLICATION

-------
	pg_databases: []
A list of database that should be created on the system. Each element must have the field `name` and optionally an `owner`. While it defaults to `postgres`, if specified must be an existing user (or present in `pg_users`)

	pg_postgres_password: postgrespassword
The admin password of `postgres` user. it is **strongly ** recommended to set this password overriding the default.

   
Dependencies
-------------------
None.

Example Playbook
--------------------------

    - name: install database
      hosts: database
      vars:
        pg_postgres_password: MYs3cur34dm1nP4ssw0rd
        pg_version: '9.4'
        pg_port: 5432
        pg_users:
            - username: newuser
              password: p4ssw0rd
              role: LOGIN
        pg_databases:
            - name: newdatabase
              owner: newuser
        
        pg_accept_connection:
            - user: newuser
              database: newdatabase
              hosts: 10.0.0.0
              mask: '/24'
              type: md5

      roles:
        - postgresql

